# INTRODUCTION #
Swagger Parser is an application that allows us to generate a Swagger JSON for a Vertx Application. When the generated JSON is added to 
Swagger Editor or made available to any Swagger Plugins then it will display a Swagger UI with all API endpoints and the user can play
around with this API to know more about the API's functionality.


### Where have we reached so far? ###
This project is just a POC so far. When we(Datamanagement team) wanted to implement swagger in our end-points, we could not find any stable
plugins for Vertx framework.


1. cd swagger-generator
1. sh swagger-parser.sh <profile> <version> <location_of_const.py_file>
1. The Swagger Json will be generated in a target directory with the name swagger.json

### What is YET to be done? ###
As mentioned above, this project was just a POC to create Swagger json for Datamanagement project. Now that, we want to decouple them apart,
these are the few problems we need to solve going forward:

* Parse classes to such a level so as to create examples and add necessary parameters inside entity.
* Add enough unit test to make it production ready.

There could be many more problems to solve as we start working on it. Please feel free to connect to Datamanagement team if further clarifications
are needed.