import re
import utils.util as util
import project_configs.const as const
import os.path


def execute(view_class, view_base_class):
    """
    Generates a swagger definition JSON.
    The input is a list of objects, where each object's key is entity name
    and the value is class name with full path of VO class
    """
    definition = dict()
    definition['type'] = 'object'
    base_properties = create_properties_for_entity(view_base_class)
    view_properties = create_properties_for_entity(view_class)
    base_properties.update(view_properties)
    definition['properties'] = base_properties
    return definition


def create_properties_for_entity(file_name):
    """ Generates a definition object for individual entity """
    if file_name is None:
        return None
    if not os.path.isfile(file_name):
        return None

    cleansified_code = util.get_cleansified_code(file_name)
    variable_statements = get_variable_statements(cleansified_code)
    definition = create_definition_object(variable_statements)
    return definition


def get_variable_statements(cleansified_code):
    """
    Takes the cleansified code list and returns a list of java class variable statements
    and not local variable statements.
    """
    data_types = '|'.join(list(const.DATA_TYPE_MAP.keys()))
    access_specfiers = '|'.join(const.JAVA_ACCESS_SPECIFIERS)
    regex = const.REG_VAR_STATEMENT.format(access_specfiers, data_types)
    statements = []
    for line in cleansified_code:
        if re.search(regex, line, re.IGNORECASE) is not None:
            statements.append(line)
    return statements


def create_definition_object(variable_statements):
    """
    Take the list that contains java variable statements and generates a list of definition objects
    in the format required for OpenAPI Spec for ONE entity
    """
    properties = {}
    for line in variable_statements:
        line_list = line.split(' ');
        name = line_list[-1]
        name = name[0:len(name)-1]
        name = util.convert_to_underscore_name(name)
        type = line_list[-2]
        properties[name] = dict()
        properties[name]["type"] = const.DATA_TYPE_MAP[type]
    return properties
