import re
import utils.util as util
import project_configs.const as const


def execute(entity, controller_class, route_method):
    """ Initial method """
    cleansified_code = util.get_cleansified_code(controller_class)
    route_method_body = util.get_body_of_a_method(cleansified_code, route_method)
    path = dict()
    path.update(create_paths(route_method_body, entity))
    return path


def create_paths(method_body, entity):
    """ creates an Uber paths object for Swagger file """
    paths = dict()
    for statement in method_body:
        http_path = get_http_path(statement, entity)
        http_method = get_http_method(statement)
        if http_path is not None and http_method is not None:
            path_object = create_path(entity, http_method, http_path)
            if http_path not in paths:
                paths[http_path] = path_object
            else:
                paths[http_path].update(path_object)
    return paths


def create_path(entity, http_method, http_path):
    """ creates an individual path to plug into Uber paths object """
    path = dict()
    path[http_method] = dict()
    path[http_method]['tags'] = [entity.capitalize()]
    path[http_method]['security'] = [{'Bearer' : []}]
    path[http_method]['produces'] = ['application/json']
    path[http_method]['responses'] = get_responses(entity, http_path)
    path[http_method]['parameters'] = get_parameters(http_path)
    return path


# Generates a parameter list for a request based on the path
def get_parameters(http_path):
    parameters = []
    temp_list_1 = http_path.split('{')
    for ele in temp_list_1:
        if '}' in ele:
            ind = ele.index('}')
            parameter = dict()
            parameter['name'] = ele[0:ind]
            parameter['in'] = 'path'
            parameter['required'] = True
            parameter['type'] = 'string'
            parameters.append(parameter)
    return parameters


def get_responses(entity, http_path):
    """ Generates a list of responses for different paths """
    responses = dict()
    responses[200] = get_response_200(entity, http_path)
    responses[400] = get_response_400()
    responses[401] = get_response_401()
    responses[404] = get_response_404()
    responses[405] = get_response_405()
    return responses


def get_response_200(entity, http_path):
    """ Generates 200 response """
    response = dict()
    response['description'] = entity + ' found'
    response['schema'] = dict()
    if entity+'s' in http_path:
        response['schema']['type'] = 'array'
        response['schema']['items'] = {'$ref':'#/definitions/'+entity}
    else:
        response['schema']['$ref'] = '#/definitions/'+entity
    return response


def get_response_400():
    """ Generates 400 response """
    response = dict()
    response['description'] = 'Invalid request. Missing required parameter.'
    return response


def get_response_401():
    """ Generates 401 response """
    response = dict()
    response['description'] = 'Unauthorized'
    return response


def get_response_404():
    """ Generates 404 response """
    response = dict()
    response['description'] = 'Not found'
    return response


def get_response_405():
    """ Generates 405 response """
    response = dict()
    response['description'] = 'Method not allowed'
    return response


def get_http_path(statement, entity):
    """ Generates a route path from a given Java code statement """
    if '/{0}'.format(entity.lower()) not in statement:
        return None
    temp_list = statement.split(entity)
    temp_list_1 = ''.join(temp_list[1:])
    temp_path_list = temp_list_1.split(")")
    path = '/{0}{1}'.format(entity, temp_path_list[0])
    path = path.replace('"','')

    ''' converting id params to Swagger format '''
    if ':' in path:
        templ_list_2 = path.split("/")
        for i in range(len(templ_list_2)):
            if ':' in templ_list_2[i]:
                templ_list_2[i] = templ_list_2[i].replace(':','')
                templ_list_2[i] = '{'+templ_list_2[i]+'}'
        path = '/'.join(templ_list_2)
    return path


def get_http_method(statement):
    """ Generates an http method from a given Java code statement """
    regex = '|'.join(const.HTTP_METHODS)
    if statement is None or re.search(regex, statement, re.IGNORECASE) is None:
        return None
    temp_list = statement.split('(')
    temp_list2 = temp_list[0].split('.')
    return temp_list2[1]

