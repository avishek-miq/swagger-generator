import os
import re
import sys
import json
import argparse
import project_configs.const as const
import doc_parsers.path_factory as path_factory
import doc_parsers.definition_factory as definition_factory

def create_definitions(entity_view_base_list):
    """ creates a full swagger definitions """
    definitions = dict()
    for element in entity_view_base_list:
        print(element)
        entity = element['entity']
        view_class = element['view-class']
        view_base_class = element['view-base-class']
        definition = definition_factory.execute(view_class, view_base_class)
        definitions[entity] = definition
    return definitions


def create_paths(entity_controller_route_list):
    """ creates a full swagger paths """
    paths = dict()
    for element in entity_controller_route_list:
        entity = element['entity']
        controller_class = element['controller-class']
        route_method = element['route-method']
        path = path_factory.execute(entity, controller_class, route_method)
        paths.update(path)
    return paths


def enhance_skeleton_template(env, ver, data):
    """THis method takes skeleton template and updates necessary information like version, environment etc"""
    env = env.lower()
    ver = ver.lower()
    if re.search(".+-snapshot$", ver) is not None:
        ver = re.sub("-snapshot.*", "", ver)
    base_path = '/v{0}'.format(ver)   
    data['host'] = const.ENVIRONMENT[env]
    data['basePath'] = base_path
    data['info']['title'] = const.TITLE
    data['info']['description'] = const.DESCRIPTION
    data['info']['contact']['email'] = const.SUPPORT_MAIL
    return data


if __name__ == '__main__':
    # Processing commandline arguments
    parser = argparse.ArgumentParser(description="Generating swagger docs")
    parser.add_argument('--env', dest='environment', help='Environment of execution (local/staging/production)',
                        default='local')
    parser.add_argument("--ver", dest="version", help="Version of the current app Eg.0.2.2-SNAPSHOT")

    arguments = parser.parse_args()
    environment = arguments.environment
    version = arguments.version    

    if environment is None or environment not in list(const.ENVIRONMENT.keys()):
        print("Please provide valid environment. Please refer to help (-h)")
        sys.exit(1)

    if version is None:
        print("The version provide is not valid. Please refer to help (-h)")
        sys.exit(1)
    

    current_path = os.path.abspath(__file__)
    current_path_list = current_path.split("/")
    project_path_list = current_path_list[0:len(current_path_list)-2]
    project_path = ('/'.join(project_path_list)+'/')

    skeleton_data = {}
    with open(project_path+const.TEMPLATE_FILE) as json_data:
        skeleton_data = json.load(json_data)

    skeleton_data = enhance_skeleton_template(environment, version, skeleton_data)

    entity_view_base = list()
    entity_controller_route = list()
   
    input = const.INPUT
    for element in input:
        entity = element['entity']
        view_class = '{0}{1}'.format(project_path, element['view-class'])
        view_class_base = '{0}{1}'.format(project_path,element['view-base-class'])
        controller_class = '{0}{1}'.format(project_path,element['controller-class'])
        route_method = element['route-method']
        entity_view_base.append({'entity':entity,'view-class':view_class, 'view-base-class':view_class_base})
        entity_controller_route.append({'entity':entity,'controller-class':controller_class, 'route-method':route_method})

    definitions = create_definitions(entity_view_base)
    paths = create_paths(entity_controller_route)
    skeleton_data['definitions'] = definitions
    skeleton_data['paths'] = paths

    if not os.path.exists(const.OUTPUT_FILE_DIR):
        os.makedirs(const.OUTPUT_FILE_DIR)

    with open(const.OUTPUT_FILE, 'w') as output_file:
        json.dump(skeleton_data, output_file, sort_keys=True, indent=const.JSON_INDENT)


