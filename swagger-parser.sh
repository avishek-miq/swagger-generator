echo "===================== SWAGGER PARSING INITIATED ==================="


ENV=$1
VERSION=$2
CONST_FILE=$3
SWAGGER_FILE='swagger.json'

echo "ENVIRONMENT $ENV"
echo "VERSION $VERSION"
echo "PROJECT CONST FILE: $CONST_FILE"

CURRENT_DIR=`pwd` # Directory to execute python script

cd ..
ROOT_DIR=`pwd` # Directory which contains apps, doc_generator, docker_files etc

cd $CURRENT_DIR
mkdir -p $CURRENT_DIR/target # since target is under git ignore
touch $CURRENT_DIR/target/$SWAGGER_FILE
cp $CONST_FILE project_configs/const.py

echo "CURRENT_DIR = $CURRENT_DIR"
echo "ROOT_DIR = $ROOT_DIR"
echo "Parsing end points ..."

# Commenting as of now as python3 has to be downgraded to 2.7
python3 start.py --env $ENV --ver $VERSION 
#status=$?

# if [ $status -ne 0 ]; then
#   echo "swagger.json is not generated. Error in generating swagger"
#   exit 1
# fi

# For time being, generating it manually as the parser has to be written with 2.7
# So to make swagger accessible via static nginx, I am generating it manually.
