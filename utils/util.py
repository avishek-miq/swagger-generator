import re
import project_configs.const as const


def get_cleansified_code(file_name):
    """ Removes all the annotations, comments, import statements, package statements """
    cleansed_code = []
    if file_name is None:
        return cleansed_code
    for line in open(file_name):
        line = line.strip()
        if line != "" and re.search(const.REG_CLEAN_CODE, line) is None:
            cleansed_code.append(line)
    return cleansed_code


def convert_to_underscore_name(name):
    """ Convert camel case name to underscore name which is used in our Json """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def print_list(l) :
    """ printing list """
    for e in l:
        print(e)


def get_body_of_a_method(cleansified_code, method_name):
    """ Algorithm: This method takes the body of a Classfile along with the name of the method.
        And it returns a body of a method as a list. """
    method_line_detected = False
    stack = []
    method_body = []
    for line in cleansified_code:
        if ' {0}'.format(method_name) in line:
            method_line_detected = True
        if method_line_detected:
            method_body.append(line)
            if '{' in line:
                stack.append('{')
            if '}' in line:
                stack.pop()
            if len(stack) == 0:
                break
    return method_body
